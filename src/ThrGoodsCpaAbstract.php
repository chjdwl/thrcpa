<?php
namespace chj\ThrCpa;

abstract class ThrGoodsCpaAbstract {

    protected $config=[];
    protected $platform = '';
    protected $instance;


    /**
     * 回调处理
     * @return mixed
     */
    abstract public function syncHandle():bool ;

    /**
     * 商品入库
     * @param array $data
     * @return bool
     */
    abstract public function store(array  $data):bool ;

    /**
     * 取数据
     * @param array $config
     * @return array
     */
    abstract public function getGoods(array $config=[]) ;


    /**
     * 二维数组指定键
     * @param array $arr
     * @param string $key
     * @return array
     */
    public function keyBy(array $arr=[],string $key=''):array {
        if (!$arr || !$key) return [];
        $data = [];
        foreach ($arr as $item){
            if (array_key_exists($key,$item)){
                $data[$item[$key]] = $item;
            }
        }
        return $data;
    }

}


