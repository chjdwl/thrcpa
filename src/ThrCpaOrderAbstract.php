<?php
namespace chj\ThrCpa;

abstract class ThrCpaOrderAbstract {

    /**
     * 回调处理
     * @param array $data
     * @return mixed
     */
    abstract public function notifyHandle(array $data):bool ;

    /**
     * 验证
     * @param array $data
     * @return bool
     */
    abstract protected  function verify(array $data):bool ;

    /**
     * 订单入库
     * @param array $data
     * @return bool
     */
    abstract protected function store(array $data):bool ;

}


