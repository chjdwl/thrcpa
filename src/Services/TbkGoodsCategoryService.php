<?php


namespace App\Services\Tbk;


use App\Modules\Manage\Model\TbGoodsCategoryModel;

class TbkGoodsCategoryService
{
    /**
     * 从淘宝客中拉取选品库数据
     * @return bool
     */
    public function getCateFromTbk()
    {
        $page     = 1;
        $pageSize = 40;
        $result   = (new TbkService('ios'))->getUatmFavorites($page, $pageSize);
        if (!isset($result->results) || !isset($result->results->tbk_favorites)) return false;
        $data = $result->results->tbk_favorites;
        $time = date('Y-m-d H:i:s');
        $this->updateOrCreateCate($data, $time);
        while (count($data) >= $pageSize) {
            $page   = $page + 1;
            $result = (new TbkService('ios'))->getUatmFavorites($page, $pageSize);
            if (!isset($result->results) || !isset($result->results->tbk_favorites)) break;
            $data = $result->results->tbk_favorites;
            $this->updateOrCreateCate($data, $time);
        }
        return true;
    }

    /**
     * 更新或新增淘宝分类数据
     * @param $data
     * @param $time
     */
    public function updateOrCreateCate($data, $time)
    {
        try {
            foreach ($data AS $key => $value) {
                $where['tb_cat_id'] = $value->favorites_id;
                $save['tb_cat_id']  = $value->favorites_id;
                $save['title']      = $value->favorites_title;
                $save['updated_at'] = $time;
                TbGoodsCategoryModel::updateOrCreate($where, $save);
            }
        } catch (\Exception $e) {
            Log::error('淘宝分类数据报错失败==='.$e->getMessage() . "\r\n" . var_export($data, true));
        }
    }

    /**
     * 获取可用选品库分类列表
     * @param int $tbCatId
     * @return mixed
     */
    public function getCanUseCateList($tbCatId=0)
    {
        $data = TbGoodsCategoryModel::select('tb_cat_id', 'title')
            ->whereNotIn('tb_cat_id', function ($query) use($tbCatId) {
                $query->from('tb_goods_category_config')->select('tb_cat_id');
                if ($tbCatId) {
                    $query->where('cat_id', '!=', $tbCatId);
                }
            })
            ->get();
        return $data;
    }
}