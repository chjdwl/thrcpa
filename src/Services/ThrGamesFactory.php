<?php
namespace chj\ThrCpa\Services;
use chj\ThrCpa\ThrCpaContainer;

class ThrGamesFactory{

    /**
     * @var
     */
    public $app;

    /**
     * 服务模块
     * @var array
     */
    protected $instance = [
        'fengling'=> 'Games\FengLing', //风铃
        'ninetyone'=> 'Games\NineTyOne', //91淘金
        'pceggs'=> 'Games\PCeggs', //pc蛋蛋
    ];

    private static  $instanceStatus;

    private function __construct(string $code,array  $parameters=[])
    {
        try{
            $instanceName = $this->instance[strtolower($code)];
        }catch (\Exception $exception) {
            throw new \Exception('不存在的支付类型');
        }
        if (class_exists($instanceName)) {
            $container = new ThrCpaContainer();
            $abstract = ucfirst($code).'Abstract';
            $container->bind($abstract,function ()use($instanceName){
                return new $instanceName;
            });
            $this->app = $container->make($abstract,$parameters);
        }else{
            throw new \Exception('获取支付数据处理服务失败');
        }
    }

    public static function getInstance(string $code,array  $parameters=[]){
        if (self::$instanceStatus){
            return self::$instanceStatus->app;
        }
        self::$instanceStatus = new self($code,$parameters);
        return  self::$instanceStatus->app;

    }


}