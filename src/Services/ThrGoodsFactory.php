<?php
namespace chj\ThrCpa\Services;
use chj\ThrCpa\ThrCpaContainer;

class ThrGoodsFactory{

    /**
     * @var
     */
    public $app;
    public $config = [];

    /**
     * 服务模块
     * @var array
     */
    protected $instance = [
        'tbk'=>'chj\ThrCpa\Goods\Taobao\Tbk\TbkService', //淘宝客
    ];

    private static  $instanceStatus;

    private function __construct(string $code,array $config=[],array $parameters=[])
    {
        try{
            $instanceName = $this->instance[strtolower($code)];
        }catch (\Exception $exception) {
            throw new \Exception('不存在的支付类型');
        }
        if (class_exists($instanceName)) {

            $newConfig = isset($config[$code]) ? $config[$code]:[];
            $newConfig = isset($newConfig[$config['platform']]) ? $newConfig[$config['platform']]:[];
            $newConfig['platform'] = $config['platform'];
            $container = new ThrCpaContainer();
            $abstract = ucfirst($code).'Abstract';
            $container->bind($abstract,function ()use($instanceName,$newConfig){
                return new $instanceName($newConfig);
            });
            $this->app = $container->make($abstract,$parameters);
        }else{
            throw new \Exception('获取支付数据处理服务失败');
        }
    }

    public static function getInstance(string $code,array $config=[],array  $parameters=[]){
        if (self::$instanceStatus){
            return self::$instanceStatus->app;
        }
        self::$instanceStatus = new self($code,$config,$parameters);
        return  self::$instanceStatus->app;

    }


}