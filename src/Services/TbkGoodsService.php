<?php
namespace chj\ThrCpa\Services;
use chj\ThrCpa\ThrGoodsCpaAbstract;
use chj\ThrCpa\Goods\Taobao\Tbk\TbkService;
use chj\ThrCpa\Services\ThrGoodsFactory;

class TbkGoodsService extends ThrGoodsCpaAbstract {

    public $thrGoodsFactory ;
    public $code = '';
    public $nowTime = '';

    public function __construct(string $code,string $platform = '', array $config = [],array $parameters=[])
    {
        $config = include_once dirname(dirname(dirname(__FILE__))).'/config.php';
        $this->config = $config['tbk']['ios'];
        $platform = 'ios';
        $this->code = $code;
        if ($platform){
            $this->platform = $config['platform'] = strtolower($platform);
        }
        $this->thrGoodsFactory = ThrGoodsFactory::getInstance($code,$config ,$parameters);
        $this->nowTime = date('Y-m-d H:i:s');
    }

    /**
     * 获取商品
     * @param array $config
     * @return array|mixed|void
     */
    public function getGoods(array $config = [])
    {
        // TODO: Implement getGoods() method.
        $data = $this->thrGoodsFactory->getGoodsDgMaterialOptional(9660);
        return json_decode(json_encode($data, 256), true);

    }

    public function store(array $data): bool
    {
        // TODO: Implement store() method.

        return  true;
    }

    public function syncHandle(): bool
    {
        $data = $this->getGoods();
        // TODO: Implement syncHandle() method.
        $goodsData = $data['result_list']['map_data'];
        //获取商品详情数据
        $itemIds     = array_column((array) $goodsData, 'item_id');
        $goodsDetail = $this->thrGoodsFactory->getGoodsInfo(implode(',', $itemIds));
        $goodsDetail    = json_decode(json_encode($goodsDetail, 256), true);
        if (!isset($goodsDetail['results']) || !isset($goodsDetail['results']['n_tbk_item']) || count($goodsDetail['results']['n_tbk_item']) <= 0) {

        }
        $goodsDetailArr = $this->keyBy($goodsDetail['results']['n_tbk_item'], 'num_iid');
        $tbkgoods =  $this->getTbkGoodsData($goodsData,$goodsDetailArr);
        $goods =  $this->getGoodsData($goodsData,$goodsDetailArr);
        $this->store($data);
    }

    public function getTbkGoodsData(array $goodsData,array $goodsDetailArr):array {
        $goods = [];
        foreach ($goodsData AS $key => $value) {
            $extImg = '';
            // 处理图片
            if (isset($value['small_images']) && isset($value['small_images']['string']) && $value['small_images']['string']) {
                $extImg = json_encode($value['small_images']['string'], JSON_UNESCAPED_UNICODE);
            } else {
                if (isset($goodsDetailArr[$value['item_id']]) &&
                    isset($goodsDetailArr[$value['item_id']]['small_images']) &&
                    isset($goodsDetailArr[$value['item_id']]['small_images']['string']) &&
                    $goodsDetailArr[$value['item_id']]['small_images']['string']
                ) {
                    $extImg = json_encode($goodsDetailArr[$value['item_id']]['small_images']['string'], JSON_UNESCAPED_UNICODE);
                }
            }

            //判断是否营销主推库商品
            $hasRebate = 0;
            if (isset($goodsDetailArr[$value['item_id']]) && isset($goodsDetailArr[$value['item_id']]['material_lib_type'])) {
                $tmp = explode(',', $goodsDetailArr[$value['item_id']]['material_lib_type']);
                if (in_array(1, $tmp)) {
                    $hasRebate = 1;
                    //计算用户佣金
                    $userReward = ($value['zk_final_price'] - $value['coupon_amount']) * ($value['commission_rate']) / 10000;
/*                    $userRate = isset($this->brokerageConfig[$catId]) ? $this->brokerageConfig[$catId]['user'] : 0;
                    $userReward = sprintf('%0.2f', (floor($userReward * $userRate)) / 100);*/
                }
            }

            //初始化数据tb_goods
            $tbTmp = [
                'goods_id' => 0,
                'tb_goods_id' => $value['item_id'],
                'tag' => 1,
                'cover' => isset($value['pict_url']) ? $value['pict_url'] : '',
                'ext_img' => $extImg,
                'tb_url' => isset($value['item_url']) ? $value['item_url'] : '',
                'provcity' => isset($goodsDetailArr[$value['item_id']]) ? isset($goodsDetailArr[$value['item_id']]['provcity']) ? $goodsDetailArr[$value['item_id']]['provcity'] : '' : '',
                'coupon_info' => isset($value['coupon_info']) ? $value['coupon_info'] : '',
                'coupon_money' => isset($value['coupon_amount']) ? $value['coupon_amount'] : '',
                'coupon_start_time' => isset($value['coupon_start_time']) ? $value['coupon_start_time'] . ' 00:00:00' : null,
                'coupon_end_time' => isset($value['coupon_end_time']) ? $value['coupon_end_time'] . ' 23:59:59' : null,
                'coupon_total_count' => isset($value['coupon_total_count']) ? $value['coupon_total_count'] : 0,
                'coupon_remain_count' => isset($value['coupon_remain_count']) ? $value['coupon_remain_count'] : 0,
                'coupon_click_url' => isset($value['coupon_share_url']) ? strpos($value['coupon_share_url'], 'http') === false ? 'https:' . $value['coupon_share_url'] : $value['coupon_share_url'] : '',
                'reward_rate' => isset($value['commission_rate']) ? sprintf('%0.4f', $value['commission_rate'] / 10000) : 0,
                'tb_cat_id' => $remoteCateId,
                'sync_time' => $this->nowTime,
                'last_sync_time' => '',
                'shop' => '',
                'updated_at' => $this->nowTime,
                'has_rebate' => $hasRebate,
                'reward' => isset($userReward) ? $userReward : 0,
                'cate_type' => 2,
            ];
            //店铺信息
            if (isset($value['shop_title']) && $value['shop_title']) {
                $shopTitle = $value['shop_title'];
            } else {
                $shopTitle = isset($goodsDetailArr[$value['item_id']]) ? $goodsDetailArr[$value['item_id']]['nick'] ? $goodsDetailArr[$value['item_id']]['nick'] : '' :'';
            }

            if ($shopTitle) {
                $shopData = $this->tbkService->getShop($shopTitle);
                if (isset($shopData->results) && $shopData->results && $shopData->total_results > 0) {
                    $shopTmp         = [
                        'name'     => $shopData->results->n_tbk_shop[0]->shop_title,
                        'cover'    => $shopData->results->n_tbk_shop[0]->pict_url,
                        'shop_url' => $shopData->results->n_tbk_shop[0]->shop_url,
                    ];
                    $shopTmp['name'] = str_replace('&#160;', ' ', $shopTmp['name']);
                    $tbTmp['shop']   = json_encode($shopTmp, JSON_UNESCAPED_UNICODE);
                } else {
                    Log::warning(get_class() . '获取商品店铺信息失败' . "\r\n" . var_export($value, true));
                    //                                    echo get_class() . '获取商品店铺信息失败' . "\r\n" . var_export($value, true). PHP_EOL;;
                    continue;
                }
            } else {
                Log::warning(get_class() . '获取商品店铺信息失败' . "\r\n" . var_export($value, true));
                //                                echo get_class() . '获取商品店铺信息失败' . "\r\n" . var_export($value, true). PHP_EOL;;
                continue;
            }
            //新增数据
            $tbTmp['created_at'] = $this->nowTime;
            $goods[] = $tbTmp;
        }
        return $goods;
    }

    public function getGoodsData(array $goodsData,array $goodsDetailArr):array {
        $goods = [];
        foreach ($goodsData AS $key => $value) {
            $extImg = '';
            // 处理图片
            if (isset($value['small_images']) && isset($value['small_images']['string']) && $value['small_images']['string']) {
                $extImg = json_encode($value['small_images']['string'], JSON_UNESCAPED_UNICODE);
            } else {
                if (isset($goodsDetailArr[$value['item_id']]) &&
                    isset($goodsDetailArr[$value['item_id']]['small_images']) &&
                    isset($goodsDetailArr[$value['item_id']]['small_images']['string']) &&
                    $goodsDetailArr[$value['item_id']]['small_images']['string']
                ) {
                    $extImg = json_encode($goodsDetailArr[$value['item_id']]['small_images']['string'], JSON_UNESCAPED_UNICODE);
                }
            }

            //判断是否营销主推库商品
            $hasRebate = 0;
            if (isset($goodsDetailArr[$value['item_id']]) && isset($goodsDetailArr[$value['item_id']]['material_lib_type'])) {
                $tmp = explode(',', $goodsDetailArr[$value['item_id']]['material_lib_type']);
                if (in_array(1, $tmp)) {
                    $hasRebate = 1;
                    //计算用户佣金
                    $userReward = ($value['zk_final_price'] - $value['coupon_amount']) * ($value['commission_rate']) / 10000;
                    /*                    $userRate = isset($this->brokerageConfig[$catId]) ? $this->brokerageConfig[$catId]['user'] : 0;
                                        $userReward = sprintf('%0.2f', (floor($userReward * $userRate)) / 100);*/
                }
            }

            //初始化数据 goods
            $goodsTmp = [
                'name'         => isset($value['title']) ? $value['title'] : '',
                'cid'          => $catId,
                'shop_price'   => isset($value['zk_final_price']) ? $value['zk_final_price'] : 0,
                'market_price' => isset($value['zk_final_price']) ? $value['zk_final_price'] : 0,
                'sales_volume' => isset($value['volume']) ? $value['volume'] : 0,
                'state'        => 1,
                'created_at'   => $this->nowTime,
                'updated_at'   => $this->nowTime,
                'from_type'    => 1,
            ];
            $goods[] = $goodsTmp;
        }
        return $goods;
    }


    /**
     * @param TbkService $obj
     * @param $catId
     * @param $materialId
     * @param $remoteCateId
     */
    private function updateOrCreateRemoteCate($catId, $materialId, $remoteCateId)
    {
        $page = 1;
        $data = [];
        do {
            try {
                //调用tbk接口获取数据
                $data = $this->thrGoodsFactory->getGoodsDgMaterialOptional($materialId, $page, 40);
                $data = json_decode(json_encode($data, 256), true);
                if (isset($data['sub_code']) && $data['sub_code'] == 30001 && $data['sub_msg'] == '非法的物料id') {
                    $this->canDeleteArr[$remoteCateId] = $remoteCateId;
                }
                //同步期间中途出错的话不删除该分类商品
                if ($page > 1 && isset($this->canDeleteArr[$remoteCateId]) && isset($data['sub_code']) && $data['sub_code'] != 50001 && $data['sub_msg'] != '无结果') {
                    unset($this->canDeleteArr[$remoteCateId]);
                    $this->unsetArr[$remoteCateId] = $remoteCateId;
                }

                if ($this->checkTbkData($data)) {
                    //有同步过的分类才进行删除和数据备份操作
                    if (!array_key_exists($remoteCateId, $this->unsetArr)) {
                        $this->canDeleteArr[$remoteCateId] = $remoteCateId;
                    }
                    $goodsData = $data['result_list']['map_data'];
                    //获取商品详情数据
                    $itemIds     = array_column((array) $goodsData, 'item_id');
                    $goodsDetail = $this->thrGoodsFactory->getGoodsInfo(implode(',', $itemIds));
                    $goodsDetail    = json_decode(json_encode($goodsDetail, 256), true);
                    if (!isset($goodsDetail['results']) || !isset($goodsDetail['results']['n_tbk_item']) || count($goodsDetail['results']['n_tbk_item']) <= 0) {
                        //Log::warning('获取商品详情异常：' . var_export($goodsDetail, true));
                                                echo '获取商品详情异常：' . var_export($goodsDetail, true). PHP_EOL;;
                        continue;
                    }
                    $goodsDetailArr = $this->keyBy($goodsDetail['results']['n_tbk_item'], 'num_iid');
                    foreach ($goodsData AS $key => $value) {
                        $extImg = '';
                        // 处理图片
                        if (isset($value['small_images']) && isset($value['small_images']['string']) && $value['small_images']['string']) {
                            $extImg = json_encode($value['small_images']['string'], JSON_UNESCAPED_UNICODE);
                        } else {
                            if (isset($goodsDetailArr[$value['item_id']]) &&
                                isset($goodsDetailArr[$value['item_id']]['small_images']) &&
                                isset($goodsDetailArr[$value['item_id']]['small_images']['string']) &&
                                $goodsDetailArr[$value['item_id']]['small_images']['string']
                            ) {
                                $extImg = json_encode($goodsDetailArr[$value['item_id']]['small_images']['string'], JSON_UNESCAPED_UNICODE);
                            }
                        }
                        //判断是否营销主推库商品
                        $hasRebate = 0;
                        if (isset($goodsDetailArr[$value['item_id']]) && isset($goodsDetailArr[$value['item_id']]['material_lib_type'])) {
                            $tmp = explode(',', $goodsDetailArr[$value['item_id']]['material_lib_type']);
                            if (in_array(1, $tmp)) {
                                $hasRebate = 1;
                                //计算用户佣金
                                $userReward = ($value['zk_final_price'] - $value['coupon_amount']) * ($value['commission_rate']) / 10000;
                               /* $userRate   = isset($this->brokerageConfig[$catId]) ? $this->brokerageConfig[$catId]['user'] : 0;
                                $userReward = sprintf('%0.2f', (floor($userReward * $userRate)) / 100);*/
                                $userReward = 0;
                            }
                        }

                        //初始化数据tb_goods
                        $tbTmp = [
                            'goods_id'            => 0,
                            'tb_goods_id'         => $value['item_id'],
                            'tag'                 => 1,
                            'cover'               => isset($value['pict_url']) ? $value['pict_url'] : '',
                            'ext_img'             => $extImg,
                            'tb_url'              => isset($value['item_url']) ? $value['item_url'] : '',
                            'provcity'            => isset($goodsDetailArr[$value['item_id']]) ? isset($goodsDetailArr[$value['item_id']]['provcity']) ? $goodsDetailArr[$value['item_id']]['provcity'] : '' : '',
                            'coupon_info'         => isset($value['coupon_info']) ? $value['coupon_info'] : '',
                            'coupon_money'        => isset($value['coupon_amount']) ? $value['coupon_amount'] : '',
                            'coupon_start_time'   => isset($value['coupon_start_time']) ? $value['coupon_start_time'] . ' 00:00:00' : null,
                            'coupon_end_time'     => isset($value['coupon_end_time']) ? $value['coupon_end_time'] . ' 23:59:59' : null,
                            'coupon_total_count'  => isset($value['coupon_total_count']) ? $value['coupon_total_count'] : 0,
                            'coupon_remain_count' => isset($value['coupon_remain_count']) ? $value['coupon_remain_count'] : 0,
                            'coupon_click_url'    => isset($value['coupon_share_url']) ? strpos($value['coupon_share_url'], 'http') === false ? 'https:' . $value['coupon_share_url'] : $value['coupon_share_url'] : '',
                            'reward_rate'         => isset($value['commission_rate']) ? sprintf('%0.4f', $value['commission_rate'] / 10000) : 0,
                            'tb_cat_id'           => $remoteCateId,
                            'sync_time'           => $this->nowTime,
                            'last_sync_time'      => '',
                            'shop'                => '',
                            'updated_at'          => $this->nowTime,
                            'has_rebate'          => $hasRebate,
                            'reward'              => isset($userReward) ? $userReward : 0,
                            'cate_type'           => 2,
                        ];
                        //初始化数据 goods
                        $goodsTmp = [
                            'name'         => isset($value['title']) ? $value['title'] : '',
                            'cid'          => $catId,
                            'shop_price'   => isset($value['zk_final_price']) ? $value['zk_final_price'] : 0,
                            'market_price' => isset($value['zk_final_price']) ? $value['zk_final_price'] : 0,
                            'sales_volume' => isset($value['volume']) ? $value['volume'] : 0,
                            'state'        => 1,
                            'created_at'   => $this->nowTime,
                            'updated_at'   => $this->nowTime,
                            'from_type'    => 1,
                        ];

                        //判断是否已存在商品
                        $hadGood = TBGoodsModel::select(['id', 'goods_id', 'tb_goods_id', 'sync_time', 'shop'])
                            ->where('tb_goods_id', $value['item_id'])
                            ->first();
                        if ($hadGood) {
                            //店铺信息
                            $shop = $hadGood->shop;
                            if ($shop) {
                                $shop          = json_decode($shop, true);
                                $shop['name']  = str_replace('&#160;', ' ', $shop['name']);
                                $tbTmp['shop'] = json_encode($shop, JSON_UNESCAPED_UNICODE);
                            }

                            //更新数据
                            $tbTmp['goods_id']       = $hadGood->goods_id;
                            $tbTmp['last_sync_time'] = $hadGood->sync_time;
                            TBGoodsModel::where('id', $hadGood->id)->update($tbTmp);
                            $hadGoodTmp = GoodsModel::where('id', $hadGood['goods_id'])->value('id');
                            if ($hadGoodTmp) {
                                GoodsModel::where('id', $hadGood->goods_id)->update($goodsTmp);
                            } else {
                                $goodsTmp['id'] = $hadGood->goods_id;
                                GoodsModel::insert($goodsTmp);
                            }

                        } else {
                            //店铺信息
                            if (isset($value['shop_title']) && $value['shop_title']) {
                                $shopTitle = $value['shop_title'];
                            } else {
                                $shopTitle = isset($goodsDetailArr[$value['item_id']]) ? $goodsDetailArr[$value['item_id']]['nick'] ? $goodsDetailArr[$value['item_id']]['nick'] : '' :'';
                            }

                            if ($shopTitle) {
                                $shopData = $this->tbkService->getShop($shopTitle);
                                if (isset($shopData->results) && $shopData->results && $shopData->total_results > 0) {
                                    $shopTmp         = [
                                        'name'     => $shopData->results->n_tbk_shop[0]->shop_title,
                                        'cover'    => $shopData->results->n_tbk_shop[0]->pict_url,
                                        'shop_url' => $shopData->results->n_tbk_shop[0]->shop_url,
                                    ];
                                    $shopTmp['name'] = str_replace('&#160;', ' ', $shopTmp['name']);
                                    $tbTmp['shop']   = json_encode($shopTmp, JSON_UNESCAPED_UNICODE);
                                } else {
                                    Log::warning(get_class() . '获取商品店铺信息失败' . "\r\n" . var_export($value, true));
                                    //                                    echo get_class() . '获取商品店铺信息失败' . "\r\n" . var_export($value, true). PHP_EOL;;
                                    continue;
                                }
                            } else {
                                Log::warning(get_class() . '获取商品店铺信息失败' . "\r\n" . var_export($value, true));
                                //                                echo get_class() . '获取商品店铺信息失败' . "\r\n" . var_export($value, true). PHP_EOL;;
                                continue;
                            }
                            //新增数据
                            $tbTmp['created_at'] = $this->nowTime;
                            $tbTmp['goods_id']   = GoodsModel::insertGetId($goodsTmp);
                            TBGoodsModel::insert($tbTmp);
                        }
                    }
                }
                usleep(500000);
            } catch (\Exception $e) {
                Log::warning('淘宝推广库商品同步报错：FILE：' . $e->getFile() . '，Line:' . $e->getLine() . '，MESSAGE:' . $e->getMessage());
                //                echo '淘宝推广库商品同步报错：FILE：' . $e->getFile() . '，Line:' . $e->getLine() . '，MESSAGE:' . $e->getMessage(). PHP_EOL;
            }
//            if (!$this->checkTbkData($data)) {
//                echo '返回数据为空' . PHP_EOL;
//            }
            $page++;
        } while ($this->checkTbkData($data));
    }

    /**
     * 检验数据
     * @param $data
     * @return bool
     */
    private function checkTbkData($data)
    {
        return isset($data['result_list']) && isset($data['result_list']['map_data']) && count($data['result_list']['map_data']) > 0;
    }

    /**
     * 备份数据
     */
    private function backupData()
    {
        $nowTime = $this->nowTime;
        $remoteCatIdArr = $this->canDeleteArr;
        if (empty($remoteCatIdArr)) {
            return false;
        }
        DB::beginTransaction();
        try {
            echo '同步淘宝商品备份开始：' . date('Y-m-d H:i:s') . PHP_EOL;
            (new ThrGoodsService())->backupTbGoods(['start_time' => $nowTime, 'cate_type' => 2, 'tb_cat_id' => $remoteCatIdArr]);
            echo '同步淘宝商品备份结束：' . date('Y-m-d H:i:s') . PHP_EOL;
            echo '删除未成功同步淘宝商品开始：' . date('Y-m-d H:i:s') . PHP_EOL;
            GoodsModel::whereIn('goods.id', function ($query) use ($nowTime, $remoteCatIdArr) {
                $query->from('tb_goods')
                    ->select('tb_goods.goods_id')
                    ->where('sync_time', '<', $nowTime)
                    ->where('cate_type', 2)
                    ->whereIn('tb_cat_id', $remoteCatIdArr);
            })->delete();
            TBGoodsModel::where('sync_time', '<', $nowTime)
                ->where('cate_type', 2)
                ->whereIn('tb_cat_id', $remoteCatIdArr)
                ->delete();
            echo '删除未成功同步淘宝商品结束：' . date('Y-m-d H:i:s') . PHP_EOL;
            DB::commit();
        } catch (\Exception $exception) {
            Log::warning(get_class() . '淘宝商品数据报错,已有商品不再删除.' . $exception->getMessage());
            DB::rollBack();
        }
    }
}