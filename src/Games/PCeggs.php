<?php
namespace chj\ThrCpa\Games;
use chj\ThrCpa\ThrGamesCpaAbstract;

class PCeggs extends ThrGamesCpaAbstract{


    private $code = 'ninetyonetaojin';

    public function __construct()
    {

    }

    /**
     * 回调
     * @param array $data
     * @return bool
     */
    public function callbackParams($data = [])
    {
        $result = ['status'=>false,];
        $resultData = [
            'success' => 0,
            'message' => '接受失败'
        ];

        if ($this->verify($data) && ($status = $this->insertDataToTable($data))) {

            if ($status == true){
                $result['status'] = true;
                $resultData = [
                    'success' => 1,
                    'message' => '接收成功'
                ];
            }
        }
        $result['data'] = json_encode($resultData,JSON_UNESCAPED_UNICODE);
        return $result;
    }

    /**
     * 数据处理
     * @param $data
     * @return bool
     */
    public function insertDataToTable($data)
    {
        DB::beginTransaction();
        try {
            $uid = $data['userid'];
            if ($user = UserModel::with([
                'detail' => function ($query) {
                    $query->select('id', 'uid', 'balance');
                }
            ])->select('id')->where('id', $uid)->first()) {
                //重复不更新
                if (EnjoyPlayCPLModel::where(['uid' => $uid, 'cpl_order_id' => $data['ordernum']])->first()) {
                    return true;
                }
                $mt_rate = isset($this->config['ios']) && isset($this->config['ios']['mt_rate']) ? $this->config['ios']['mt_rate'] : 1;
                $companyFee = sprintf('%.2f', (($data['price'] * $mt_rate) - $data['money']));
                $order_sn = $this->createOrderNo();
                $cpl = [
                    'uid' => $uid,
                    'pid'=> (isset($data['pid']) && $data['pid']) ? $data['pid'] : '',
                    'order_sn' => $order_sn,
                    'cpl_order_id' => (isset($data['ordernum']) && $data['ordernum']) ? $data['ordernum'] : '',
                    'adid' => (isset($data['pid']) && $data['pid']) ? $data['pid'] : '',
                    'adname' => (isset($data['adname']) && $data['adname']) ? $data['adname'] : '',
                    'dlevel' => (isset($data['dlevel']) && $data['dlevel']) ? $data['dlevel'] : '',
                    'pagename' => (isset($data['pagename']) && $data['pagename']) ? $data['pagename'] : '',
                    'deviceid' => (isset($data['deviceid']) && $data['deviceid']) ? $data['deviceid'] : '',
                    'simid' => (isset($data['simid']) && $data['simid']) ? $data['simid'] : '',
                    'merid' => (isset($data['merid']) && $data['merid']) ? $data['merid'] : '',
                    'price' => (isset($data['price']) && $data['price']) ? $data['price'] : 0,
                    'money' => (isset($data['money']) && $data['money']) ? $data['money'] : 0,
                    'itime' => (isset($data['itime']) && $data['itime']) ? $data['itime'] : '',
                    'note'  => (isset($data['event']) && $data['event']) ? $data['event'] : '',
                ];
                //cpl记录
                $gameID = EnjoyPlayCPLModel::insertGetId($cpl);
                $now = date('Y-m-d H:i:s');
                $funding = [
                    'uid' => $uid,
                    'order_id' => $order_sn,
                    'action' => 3,
                    'way' => 0,
                    'account_type' => 0,
                    'account_number' => '',
                    'money' => $data['money'],
                    'fees' => 0,
                    'balance' => sprintf('%.2f', ($user->detail->balance + $data['money'])),
                    'remark' => '奕天网络',
                    'created_at' => $now,
                    'completed_at' => $now,
                ];
                //个人资金明细
                $fid =  FundingDetailsModel::insertGetId($funding);
                $company = EnterpriseModel::where('uid', 1)->first();
                $funding['uid'] = 1;
                $funding['account_type'] = 1;
                $funding['money'] = $companyFee;
                $funding['balance'] = sprintf('%.2f', ($company->balance + $companyFee));
                //企业资金明细
                FundingDetailsModel::insert($funding);
                //个人余额
                UserDetailModel::where('uid', $uid)->increment('balance', $data['money']);
                //企业余额
                EnterpriseModel::where('uid', 1)->increment('balance', $companyFee);
                DB::commit();
                //资金变动
                (new MessagesService())->sendCapitalChangeMessage($uid,$data['money'],$fid);
                return true;
            }
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('享玩平台回调新增数据失败' . get_class() . ':' . $e->getMessage() . '回调参数：' . json_encode($data,
                    JSON_UNESCAPED_UNICODE));
            return false;
        }
        return false;
    }

    /**
     * 订单号
     */
    public function createOrderNo()
    {
        $hash = \CommonClass::mobileRand(7, 1);
        $code = 'PCDD' . date('Ymd') . $hash;
        if (EnjoyPlayCPLModel::where('order_sn', $code)->first()) {
            return $this->createOrderNo();
        }
        return $code;
    }


    /**
     * pc蛋蛋验签
     * @param $data
     * @param $sign
     * @return bool
     */
    public function verify($data){
        if (!$data || !is_array($data)) {
            return false;
        }
        $data['appkey'] =  isset($this->config['ios']) && isset($this->config['ios']['appkey']) ? $this->config['ios']['appkey'] : (isset($this->config['android']) && isset($this->config['android']['appkey']) ? $this->config['android']['appkey']:'');
        $ruleKey = [
            'adid','pid','ordernum','deviceid','appkey'
        ];
        if (!isset($data['keycode'])) return false;
        $keycode = $data['keycode'];
        $sign = '';
        foreach ($ruleKey as $key){
            if (!array_key_exists($key,$data)){
                return false;
            }else{
                $sign .= $data[$key];
            }
        }
        if ($sign){
            if (strtolower(md5($sign)) == $keycode){
                return true;
            }
            Log::error(get_class().'PC蛋蛋验签失败，参数:'.json_encode($data,256));
            return false;
        }
        return false;
    }
}
