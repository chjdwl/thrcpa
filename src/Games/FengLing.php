<?php
namespace chj\ThrCpa\Games;
use chj\ThrCpa\ThrGamesCpaAbstract;

class FengLing extends ThrGamesCpaAbstract {


    private $code = 'ninetyonetaojin';

    public function __construct()
    {

    }

    /**
     * cpl回调
     * @param array $data
     * @return bool
     */
    public function callbackParams($data = [])
    {
        $result = [
            'status'=>false,
            'data'=> '',
        ];
        if ($this->verify($data) && ($status = $this->insertDataToTable($data))) {
            if ($status == true){
                $result['status'] = true;
                $result['data'] = 'ok';
            }
        }
        return $result;
    }

    /**
     * cpl数据处理
     * @param $data
     * @return bool
     */
    public function insertDataToTable($data)
    {
        $mt_rate = isset($this->config['ios']) && isset($this->config['ios']['mt_rate']) ? $this->config['ios']['mt_rate'] : 0.8;
        if (isset($data['Note']) && $data['Note']) {
            $data['Note'] = urldecode($data['Note']);
        }
        $ext_params = '';
        $companyFee = sprintf('%.2f', (($data['MtFee'] * $mt_rate) - $data['UserFee']));
        if (array_key_exists('ExtParams',$data)){
            try{
                $extParams = json_decode(urldecode($data['ExtParams']),1);
                $ext_params = json_encode($extParams,256);
                if (is_array($extParams) && isset($extParams['MtGetFee'])){
                    $companyFee = sprintf('%.2f', ($extParams['MtGetFee']  - $data['UserFee']));
                }else{
                    Log::error('91cpl回调新增数据失败，ExtParams中MtGetFee不存在' . get_class() . ':回调参数：' . json_encode($data,
                            JSON_UNESCAPED_UNICODE));
                    return false;
                }
            }catch (\Exception $exception){
                Log::error('91cpl回调新增数据失败,ExtParams格式不对' . get_class() . ':' . $exception->getMessage() . '回调参数：' . json_encode($data,
                        JSON_UNESCAPED_UNICODE));
                return false;
            }
        }
        if ($companyFee < 0){
            Log::error('91cpl回调新增数据失败，公司收入为负数' . get_class() . ':回调参数：' . json_encode($data,
                    JSON_UNESCAPED_UNICODE));
            return false;
        }
        DB::beginTransaction();
        try {
            $uid = $data['MtIDUser'];
            if ($user = UserModel::with([
                'detail' => function ($query) {
                    $query->select('id', 'uid', 'balance');
                }
            ])->select('id')->where('id', $uid)->first()) {
                //重复不更新
                if (GameCPLModel::where(['uid' => $uid, 'cpl_order_id' => $data['ID']])->first()) {
                    return true;
                }
                $order_sn = $this->createOrderNo();
                $cpl = [
                    'uid' => $uid,
                    'order_sn' => $order_sn,
                    'cpl_order_id' => $data['ID'],
                    'platform' => (isset($data['IMEI']) && $data['IMEI']) ? 'android' : (isset($data['IDFA']) && $data['IDFA'] ? 'ios' : ''),
                    'platform_code' => (isset($data['IMEI']) && $data['IMEI']) ? $data['IMEI'] : (isset($data['IDFA']) && $data['IDFA'] ? $data['IDFA'] : ''),
                    'id_user' => $data['IDUser'],
                    'user_fee' => $data['UserFee'],
                    'mt_fee' => $data['MtFee'],
                    'done_time' => $data['DoneTime'],
                    'id_task' => $data['IDTask'],
                    'note' => $data['Note'],
                    'ext_params'=>$ext_params,
                ];
                //cpl记录
                $gameID = GameCPLModel::insertGetId($cpl);
                $now = date('Y-m-d H:i:s');
                $funding = [
                    'uid' => $uid,
                    'order_id' => $order_sn,
                    'action' => 3,
                    'way' => 0,
                    'account_type' => 0,
                    'account_number' => '',
                    'money' => $data['UserFee'],
                    'fees' => 0,
                    'balance' => sprintf('%.2f', ($user->detail->balance + $data['UserFee'])),
                    'remark' => '91淘金CPL奖励',
                    'created_at' => $now,
                    'completed_at' => $now,
                ];
                //个人资金明细
                $fid =  FundingDetailsModel::insertGetId($funding);
                $company = EnterpriseModel::where('uid', 1)->first();
                $funding['uid'] = 1;
                $funding['account_type'] = 1;
                $funding['money'] = $companyFee;
                $funding['balance'] = sprintf('%.2f', ($company->balance + $companyFee));
                //企业资金明细
                FundingDetailsModel::insert($funding);
                //个人余额
                UserDetailModel::where('uid', $uid)->increment('balance', $data['UserFee']);
                //企业余额
                EnterpriseModel::where('uid', 1)->increment('balance', $companyFee);
                DB::commit();
                //资金变动
                (new MessagesService())->sendCapitalChangeMessage($uid,$data['UserFee'],$fid);
                return true;
            }
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('cpl回调新增数据失败' . get_class() . ':' . $e->getMessage() . '回调参数：' . json_encode($data,
                    JSON_UNESCAPED_UNICODE));
            return false;
        }
        return false;
    }

    /**
     * 订单号
     */
    public function createOrderNo()
    {
        $hash = \CommonClass::mobileRand(7, 1);
        $code = 'CPL' . date('Ymd') . $hash;
        if (GameCPLModel::where('order_sn', $code)->first()) {
            return $this->createOrderNo();
        }
        return $code;
    }


    /**
     * 91平台验签
     * @param $data
     * @return bool
     */
    public function verify($data){
        if (!$data || !is_array($data)) {
            return false;
        }
        $AppKey = '';
        $keyArr = [];
        $rule = [
            'DoneTime',
            'ExtParams',
            'ID',
            'IDTask',
            'IDUser',
            'MtFee',
            'MtIDUser',
            'MtId',
            'Note',
            'UserFee',
        ];
        if (isset($data['IDFA']) && $data['IDFA']) {
            $rule[] =  'IDFA';
            $AppKey = $this->config['ios']['AppKey'];
        } elseif (isset($data['IMEI']) && $data['IMEI']) {
            $rule[] = 'IMEI';
            $AppKey = $this->config['android']['AppKey'];
        }
        if (isset($data['Note']) && $data['Note']) {
            $data['Note'] = urldecode($data['Note']);
        }
        if (isset($data['ExtParams']) && $data['ExtParams']) {
            $data['ExtParams'] = urldecode($data['ExtParams']);
        }
        sort($rule);
        foreach ($rule as $item) {
            if (isset($data[$item])) {
                $keyArr[] = $data[$item];
            }
        }
        $keyArr[] = $AppKey;

        if ($data['sign'] == md5(base64_encode(implode('#', $keyArr)))){
            return true;
        }
        Log::error(get_class().'91平台验签失败，参数:'.json_encode($data,256));
        return false;
    }
}
