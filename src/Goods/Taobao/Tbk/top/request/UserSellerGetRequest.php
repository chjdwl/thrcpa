<?php


class UserSellerGetRequest
{
    public $fields = '';
    private $apiParas = array();

    public function setFields($fields)
    {
        $this->fields = $fields;
        $this->apiParas["random_num"] = $fields;
    }
    public function getApiMethodName()
    {
        return "taobao.user.seller.get";
    }

    public function getApiParas()
    {
        return $this->apiParas;
    }

    public function check()
    {

    }

    public function putOtherTextParam($key, $value) {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }
}