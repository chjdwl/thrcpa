<?php


class TaobaoTaeItemsList
{
    /**
     * 返回值中需要的字段
     **/
    private $fields;

    /**
     * 商品ID
     **/
    private $numIids;

    /**
     * 商品混淆ID
     **/
    private $openIids;

    private $apiParas = array();

    public function setFields($fields)
    {
        $this->fields = $fields;
        $this->apiParas["fields"] = $fields;
    }


    public function setNumIids($numIids)
    {
        $this->numIids = $numIids;
        $this->apiParas["num_iids"] = $numIids;
    }

    public function getNumIids()
    {
        return $this->numIids;
    }

    public function setOpenIids($openIids)
    {
        $this->openIids = $openIids;
        $this->apiParas["open_iids"] = $openIids;
    }

    public function getOpenIids()
    {
        return $this->openIids;
    }

    public function getApiMethodName()
    {
        return "taobao.tae.items.list";
    }

    public function getApiParas()
    {
        return $this->apiParas;
    }

    public function check()
    {
        RequestCheckUtil::checkNotNull($this->numIids,"fields");
    }

    public function putOtherTextParam($key, $value) {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }
}