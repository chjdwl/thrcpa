<?php
/**
 * TOP API: taobao.tbk.item.info.get request
 *
 * @author auto create
 * @since 1.0, 2018.11.10
 */
class TbkItemShareConvertRequest
{
    /**
     * 三方pid，满足mm_xxx_xxx_xxx格式
     **/
    private $subPid;

    /**
     * 商品ID串，用,分割，最大40个
     **/
    private $numIids;

    /**
     * 需返回的字段列表
     **/
    private $fields;

    /**
     * 链接形式：1：PC，2：无线，默认：１
     **/
    private $platform;

    /**
     * 自定义输入串，英文和数字组成，长度不能大于12个字符，区分不同的推广渠道
     **/
    private $unid;

    /**
     * 广告位ID，区分效果位置
     **/
    private $adzoneId;

    private $apiParas = array();

    public function setsubPid($subPid)
    {
        $this->subPid = $subPid;
        $this->apiParas["sub_pid"] = $subPid;
    }

    public function setNumIids($numIids)
    {
        $this->numIids = $numIids;
        $this->apiParas["num_iids"] = $numIids;
    }

    public function setPlatform($platform)
    {
        $this->platform = $platform;
        $this->apiParas["platform"] = $platform;
    }

    public function setFields($fields)
    {
        $this->fields = $fields;
        $this->apiParas["fields"] = $fields;
    }

    public function setUnid($unid)
    {
        $this->unid = $unid;
        $this->apiParas["unid"] = $unid;
    }

    public function setAdzoneId($adzoneId)
    {
        $this->adzoneId = $adzoneId;
        $this->apiParas["adzone_id"] = $adzoneId;
    }

    public function getApiMethodName()
    {
        return "taobao.tbk.item.share.convert";
    }

    public function getApiParas()
    {
        return $this->apiParas;
    }

    public function check()
    {

        RequestCheckUtil::checkNotNull($this->numIids,"fields");
        RequestCheckUtil::checkNotNull($this->numIids,"numIids");
        RequestCheckUtil::checkNotNull($this->numIids,"subPid");
        RequestCheckUtil::checkNotNull($this->numIids,"adzoneId");
    }

    public function putOtherTextParam($key, $value) {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }
}
