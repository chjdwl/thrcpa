<?php


class FeedflowItemCampaignPageRequest
{
    /**
     * 券ID
     **/
    private $activityId;

    /**
     * 商品ID
     **/
    private $itemId;

    /**
     * 带券ID与商品ID的加密串
     **/
    private $me;

    private $apiParas = array();

    public function setCampaignId($activityId)
    {
        $this->activityId = $activityId;
        $this->apiParas["campaign_id"] = $activityId;
    }


    public function setItemId($itemId)
    {
        $this->itemId = $itemId;
        $this->apiParas["item_id"] = $itemId;
    }

    public function getItemId()
    {
        return $this->itemId;
    }

    public function setMe($me)
    {
        $this->me = $me;
        $this->apiParas["me"] = $me;
    }

    public function getMe()
    {
        return $this->me;
    }

    public function getApiMethodName()
    {
        return "taobao.feedflow.item.campaign.page";
    }

    public function getApiParas()
    {
        return $this->apiParas;
    }

    public function check()
    {

    }

    public function putOtherTextParam($key, $value) {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }
}