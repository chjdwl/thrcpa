<?php
/**
 * TOP API: taobao.openuid.get request
 *
 * @author auto create
 * @since 1.0, 2018.10.17
 */
class TaobaoOpeniidGetRequest
{
    private $itemId ;
    private $apiParas = array();

    public function getApiMethodName()
    {
        return "taobao.openiid.get";
    }

    public function getApiParas()
    {
        return $this->apiParas;
    }

    public function setItemId($itemId)
    {
        $this->itemId = $itemId;
        $this->apiParas['item_id'] = $itemId;
    }

    public function check()
    {
        RequestCheckUtil::checkNotNull($this->itemId,"item_id");
    }

    public function putOtherTextParam($key, $value) {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }
}
