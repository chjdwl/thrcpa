CREATE TABLE `xm_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '商品名称',
  `cid` int(11) DEFAULT NULL COMMENT '分类ID',
  `sid` int(11) DEFAULT NULL COMMENT '供应商ID',
  `sn` varchar(255) DEFAULT NULL COMMENT '商品货号',
  `unit` char(20) DEFAULT NULL COMMENT '商品单位',
  `price` decimal(10,2) DEFAULT '0.00' COMMENT '成本价',
  `shop_price` decimal(10,2) DEFAULT '0.00' COMMENT '销售价',
  `market_price` decimal(10,2) DEFAULT '0.00' COMMENT '市场价',
  `is_new` tinyint(1) DEFAULT '0' COMMENT '是否新品 0 否，1是',
  `is_best` tinyint(1) DEFAULT '0' COMMENT '是否精品 0 否，1是',
  `is_hot` tinyint(1) DEFAULT '0' COMMENT '是否热销 0 否，1是',
  `cover` int(11) DEFAULT NULL COMMENT '商品主图',
  `ext_img` varchar(255) DEFAULT NULL COMMENT '扩展图片（如1,2,3）',
  `sales_volume` int(11) DEFAULT '0' COMMENT '销量',
  `stock` int(11) DEFAULT '0' COMMENT '库存',
  `weight` int(11) DEFAULT '0' COMMENT '商品重量（以克为最小单位）',
  `warn_number` int(11) DEFAULT '0' COMMENT '库存报警数量',
  `state` tinyint(2) DEFAULT '0' COMMENT '商品状态（0：已保存、1：已上架、2：已下架）',
  `created_at` timestamp NULL DEFAULT NULL COMMENT '添加时间',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `start_sales_volume` int(11) NOT NULL DEFAULT '0' COMMENT '基础销量(起始销量)',
  `view_num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '商品点击量',
  `from_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '商品来源类别',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_from_type` (`from_type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=167810 DEFAULT CHARSET=utf8 COMMENT='商品表';

CREATE TABLE `xm_tb_goods` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `goods_id` int(11) unsigned NOT NULL COMMENT '商品表ID',
  `tb_goods_id` bigint(15) unsigned NOT NULL COMMENT '淘宝商品ID',
  `tag` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '标签，1=淘宝，2=天猫',
  `cover` varchar(255) NOT NULL DEFAULT '' COMMENT '商品主图',
  `ext_img` varchar(2048) NOT NULL DEFAULT '' COMMENT '扩展图片',
  `tb_url` varchar(2048) NOT NULL DEFAULT '' COMMENT '商品的淘宝地址',
  `provcity` varchar(30) NOT NULL DEFAULT '' COMMENT '商品所在地',
  `coupon_info` varchar(20) NOT NULL DEFAULT '' COMMENT '优惠券面额描述（如：满16元减10元）',
  `coupon_money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '优惠券面额',
  `coupon_start_time` timestamp NULL DEFAULT NULL COMMENT '优惠券开始时间',
  `coupon_end_time` timestamp NULL DEFAULT NULL COMMENT '优惠券结束时间',
  `coupon_total_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '优惠券总量',
  `coupon_remain_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '优惠券剩余量',
  `coupon_click_url` varchar(2048) NOT NULL DEFAULT '' COMMENT '商品优惠券推广链接',
  `shop` varchar(1024) NOT NULL DEFAULT '' COMMENT '店铺信息',
  `reward_rate` decimal(8,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT '收入比例',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sync_time` timestamp NULL DEFAULT NULL COMMENT '最新同步时间',
  `last_sync_time` timestamp NULL DEFAULT NULL COMMENT '上次同步时间',
  `tb_cat_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'xm_tb_goods_category.id或xm_tb_remote_cate.id',
  `has_rebate` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否有返佣 1有 0否',
  `reward` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '商品佣金',
  `cate_type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '分类类型，1-关联xm_tb_goods_category，2-关联xm_tb_remote_cate',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_goods_id` (`goods_id`) USING BTREE,
  KEY `idx_tb_cat_id` (`tb_cat_id`) USING BTREE,
  KEY `idx_tb_goods_id` (`tb_goods_id`) USING BTREE,
  KEY `idx_reward` (`reward`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=167607 DEFAULT CHARSET=utf8 COMMENT='淘宝商品附加表';


CREATE TABLE `xm_tb_goods_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `tb_cat_id` int(11) NOT NULL DEFAULT '0' COMMENT '淘宝选品库分类ID',
  `title` varchar(200) NOT NULL DEFAULT '' COMMENT '分类名称',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` timestamp NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_tb_cat_id` (`tb_cat_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COMMENT='淘宝选品库分类表';

CREATE TABLE `xm_tb_goods_category_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `cat_id` int(11) NOT NULL DEFAULT '0' COMMENT '分类ID，对应商品分类表id',
  `tb_cat_id` int(11) NOT NULL DEFAULT '0' COMMENT '淘宝选品库分类ID',
  `cate_type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '分类类型，1-关联xm_tb_goods_category，2-关联xm_tb_remote_cate',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` timestamp NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_cat_id` (`cat_id`) USING BTREE,
  KEY `idx_tb_cat_id` (`tb_cat_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8 COMMENT='淘宝选品库分类设置表';


CREATE TABLE `xm_tb_remote_cate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `type_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'xm_tb_remote_type.id',
  `name` varchar(20) NOT NULL DEFAULT '' COMMENT '类型名称',
  `material_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '淘宝物料id',
  `created_at` timestamp NOT NULL COMMENT '创建时间',
  `updated_at` timestamp NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_type_id` (`type_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='淘宝推广商品分类表';


CREATE TABLE `xm_tb_remote_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `name` varchar(20) NOT NULL DEFAULT '' COMMENT '类型名称',
  `created_at` timestamp NOT NULL COMMENT '创建时间',
  `updated_at` timestamp NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='淘宝推广商品类型表';