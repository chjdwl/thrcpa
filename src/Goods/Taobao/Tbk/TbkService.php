<?php
namespace chj\ThrCpa\Goods\Taobao\Tbk;

use TbkDgOptimusMaterialRequest;

require_once  "TopSdk.php";

class TbkService
{
    public $tbk = null;
    public $config = [];

    public function __construct(array $config=[])
    {
        $this->config= $config;
        $tbk = new \TopClient();
        $tbk->appkey = isset($config['appkey']) ?$config['appkey']: '';
        $tbk->secretKey = isset($config['appsecret']) ?$config['appsecret']: '';
        $tbk->format = 'json';
        $this->tbk = $tbk;
        $this->platform =isset($config['platform']) ? $config['platform']: '';
    }

    /**
     * 获取选品库列表
     * @param $page
     * @param $pageSize
     * @return object
     */
    public function getUatmFavorites($page=1, $pageSize=40)
    {
        $req = new \TbkUatmFavoritesGetRequest();
        $req->setPageNo($page);
        $req->setPageSize($pageSize);
        $req->setFields("favorites_title,favorites_id,type");
        $req->setType("1");
        $resp = $this->tbk->execute($req);
        return $resp;
    }

    /**
     * 获取选品库宝贝信息
     * @param $favoritesId
     * @param $uid
     * @param int $page
     * @param int $pageSize
     * @return object
     */
    public function getUatmFavoritesItem($favoritesId, $uid=0, $page=1, $pageSize=40)
    {
        $req = new \TbkUatmFavoritesItemGetRequest();
        $req->setPlatform("1");
        $req->setPageNo($page);
        $req->setPageSize($pageSize);
        $req->setAdzoneId($this->getAdzoneId());
        $req->setFavoritesId($favoritesId);
        if ($uid){
            $req->setUnid($uid);
        }
        $req->setFields("item_description,coupon_end_time,coupon_start_time,coupon_total_count,coupon_remain_count,coupon_info,coupon_click_url,click_url,num_iid,title,pict_url,small_images,reserve_price,zk_final_price,user_type,provcity,item_url,seller_id,volume,nick,shop_title,zk_final_price_wap,event_start_time,event_end_time,tk_rate,status,type");
        $resp = $this->tbk->execute($req);
        return $resp;
    }

    /**
     * 获取淘口令
     * @param $text
     * @param $url
     * @param $logo
     * @return object
     */
    public function getTpwd($text, $url, $logo='')
    {
        $req = new \TbkTpwdCreateRequest();
        $req->setText($text);
        $req->setUrl($url);
        $req->setLogo($logo);
        $resp = $this->tbk->execute($req);
        return $resp;
    }

    /**
     * 获取商品关联推荐
     * @param $numIid
     * @param int $count
     * @return object
     */
    public function getItemRecommend($numIid, $count=40)
    {
        $req = new \TbkItemRecommendGetRequest();
        $req->setNumIid($numIid);
        $req->setFields("num_iid,title,pict_url,small_images,reserve_price,zk_final_price,user_type,provcity,item_url,nick,seller_id,volume");
        $req->setCount($count);
        $resp = $this->tbk->execute($req);
        return $resp;
    }

    /**
     * 获取店铺搜索
     * @param $shopName
     * @param int $page
     * @param int $pageSize
     * @return object
     */
    public function getShop($shopName, $page=1, $pageSize=40)
    {
        $req = new \TbkShopGetRequest();
        $req->setFields("user_id,shop_title,shop_type,seller_nick,pict_url,shop_url");
        $req->setQ($shopName);
        $req->setPageNo($page);
        $req->setPageSize($pageSize);
        $resp = $this->tbk->execute($req);
        return $resp;
    }

    /**
     * 获取订单列表
     * @param $startTime
     * @param $endTime
     * @param $type 1-常规订单，2-渠道订单，3-会员运营订单
     * @param int $page
     * @param int $pageSize
     * @param string $positionIndex
     * @return object
     */
    public function getOrdersList($startTime, $endTime, $type=1, $page=1, $pageSize=40, $positionIndex='')
    {
        $req = new \TbkOrderDetailGetRequest();
        $req->setStartTime($startTime);
        $req->setEndTime($endTime);
        $req->setOrderScene($type);
        $req->setPageNo($page);
        $req->setPageSize($pageSize);
        $positionIndex && $req->setPositionIndex($positionIndex);
        $resp = $this->tbk->execute($req);
        return $resp;
    }

    /**
     * 商品open id 获取
     * @param $itemId
     * @return object
     */
    public function getOpeniid($itemId)
    {
        $req = new \TaobaoOpeniidGetRequest();
        $req->setItemId($itemId);
        $resp = $this->tbk->execute($req);
        return $resp;
    }

    /**
     * 通过淘宝商品id获取推广信息
     * @param $numIids
     * @return object
     */
    public function getItemShareConvert($numIids)
    {
        $req = new \TbkItemShareConvertRequest();
        $req->setNumIids($numIids);
        $req->setAdzoneId($this->getAdzoneId());
        $req->setFields('num_iid,click_url,commission_rate,coupon_amount,coupon_click_url');
        $pidArr = config('tbk.'.$this->platform.'.pid');
        $pid = isset($pidArr[0]) ? $pidArr[0] : '';
        $req->setsubPid($pid);
        $resp = $this->tbk->execute($req);
        return $resp;
    }

    /**
     * TAE获取商品列表
     * @param string $goodsId
     * @param string $openIids
     * @return object
     */
    public function getGoodsListByTae($goodsId='', $openIids='')
    {
        $req = new \TaobaoTaeItemsList();
        $req->setFields("title,nick,price");
        $req->setNumIids($goodsId);
        $req->setOpenIids($openIids);
        $resp = $this->tbk->execute($req);
        return $resp;
    }

    /**
     * TAE获取商品列表
     * @param string $numIids
     * @param string $openIids
     * @return object
     */
    public function getGoodsDetailByTae($openIids='')
    {
        $req = new \TaobaoTaeItemsDetail();
        $req->setFields("itemInfo,priceInfo,skuInfo,stockInfo,rateInfo,descInfo,sellerInfo,mobileDescInfo,deliveryInfo,storeInfo,itemBuyInfo,couponInfo");
        $req->setOpenIids($openIids);
        $resp = $this->tbk->execute($req);
        return $resp;
    }

    /**
     * 获取商品详情
     * @param $goodsId
     * @return mixed|\ResultSet|\SimpleXMLElement
     */
    public function getGoodsInfo($goodsId)
    {
        $req = new \TbkItemInfoGetRequest();
        $req->setNumIids($goodsId);
        $resp = $this->tbk->execute($req);
        return $resp;
    }

    /**
     * 物料精选
     * @param $goodsId
     * @param $page
     * @param $pageSize
     * @return object
     */
    public function getGoodsDgOptimusMaterial($goodsId, $page=1, $pageSize=10)
    {
        $req = new TbkDgOptimusMaterialRequest();
        $req->setAdzoneId($this->getAdzoneId());
        $req->setItemId($goodsId);
        $req->setPageSize($pageSize);
        $req->setPageNo($page);
        $req->setMaterialId("6708"); //写死的官方物料id
        $resp = $this->tbk->execute($req);
        return $resp;
    }

    /**
     * 通用物料搜索
     * @param $materialId
     * @param int $page
     * @param int $pageSize
     * @return mixed|\ResultSet|\SimpleXMLElement
     */
    public function getGoodsDgMaterialOptional($materialId, $page=1, $pageSize=10)
    {
        $req = new \TbkDgMaterialOptionalRequest();
        $req->setAdzoneId($this->getAdzoneId());
        $req->setPageSize($pageSize);
        $req->setPageNo($page);
        $req->setCat(0);
        $req->setMaterialId($materialId);
        $resp = $this->tbk->execute($req);
        return $resp;
    }

    /**
     * 用户绑定渠道
     * @param $accessToken
     * @return \ResultSet
     */
    public function bindChannel($accessToken)
    {
        $req = new \TbkScPublisherInfoSaveRequest();
        $req->setInviterCode(config('tbk.channel_invite_code')); //https://mos.m.taobao.com/inviter/register?inviterCode=T7VWGP&src=pub&app=common
        $req->setInfoType(1);
        $resp = $this->tbk->execute($req, $accessToken);
        return $resp;
    }

    /**
     * 绑定用户运营
     * @param $accessToken
     * @return \ResultSet
     */
    public function bindUser($accessToken)
    {
        $req = new \TbkScPublisherInfoSaveRequest();
        $req->setInviterCode(config('tbk.user_invite_code')); //https://mos.m.taobao.com/inviter/register?inviterCode=T7VWGP&src=pub&app=common
        $req->setInfoType(1);
        $resp = $this->tbk->execute($req, $accessToken);
        return $resp;
    }

    /**
     * 验证授权签名
     * @param $url
     * @return array|bool
     */
    public function checkAuthSign($url)
    {
        if (!$url) return false;
        $urlArr = explode('#', $url);
        if (!isset($urlArr[1])) return false;
        $uri = explode('&', $urlArr[1]);
        $arr = [];
        foreach ($uri AS $value) {
            $tempArr = explode('=', $value);
            if (!isset($tempArr[1]) || $tempArr[1] === '') continue;
            $arr[$tempArr[0]] = $tempArr[1];
        }
        $tbSign = $arr['top_sign'];
        unset($arr['top_sign']);
        ksort($arr);
        $str = $this->tbk->secretKey;
        foreach ($arr AS $key=>$value) {
            $str .= $key . $value;
        }
        $str .= $this->tbk->secretKey;
        if (strtoupper(md5($str)) == $tbSign) {
            return $arr;
        }
        return false;
    }

    /**
     * 获取授权链接
     * @return string
     */
    public function getAuthClientUrl()
    {
        $appkey = $this->tbk->appkey;
        $url = 'https://oauth.taobao.com/authorize?response_type=token&client_id='. $appkey .'&state=xmapp&view=wap';
        return $url;
    }

    /**
     * 获取adzoneId
     * @return string
     */
    public function getAdzoneId()
    {
        $pidArr = isset($this->config['pid'])?$this->config['pid']:[];
        $pid = isset($pidArr[0]) ? $pidArr[0] : '';
        if (!$pid) return '';
        $adZoneId = explode('_', $pid);

        return isset($adZoneId[3]) ? $adZoneId[3] : '';
    }
}