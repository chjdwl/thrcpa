## contos 编译安装mysql-8.*
### 安装依赖
- yum install -y  gcc gcc-c++ cmake ncurses ncurses-devel bison
### 官方下载含boost的源码包
- wget https://cdn.mysql.com//Downloads/MySQL-*/mysql-boost-*.tar.gz
### 添加mysql用户
- useradd -s /sbin/nologin mysql

### 建立mysql安装目录及存放数据目录
- mkdir -p /web/server/mysql/data
### 分配mysql目录权限
- chown -R mysql:mysql /web/server/mysql
### 解压下载源码包
- tar -zxvf mysql-boost-*.tar.gz -C /web/server/mysql
### 进入解压目录进行编译安装
- cd /web/server/mysql
- cmake . -DCMAKE_INSTALL_PREFIX=/web/server/mysql/ -DINSTALL_DATADIR=/web/server/mysql/data/master -DSYSCONFDIR=/web/server/mysql/etc/my.cnf -DFORCE_INSOURCE_BUILD=1 -DDOWNLOAD_BOOST=1 -DWITH_BOOST=/web/src/boost_1_70_0  -DDEFAULT_CHARSET=utf8 -DWITH_MYISAM_STORAGE_ENGINE=1 -DMYSQL_USER=mysql
- make && make install 
### 内存不足添加临时交换分区获取要增加的2G的SWAP文件块
- dd if=/dev/zero of=/swapfile bs=1k count=4096000
- 创建SWAP文件
- - mkswap /swapfile 
- 激活SWAP文件
- - swapon /swapfile   
- 查看SWAP信息是否正确
- - swapon -s  
- 添加到fstab文件中让系统引导时自动启动
- - echo "/var/swapfile swap swap defaults 0 0" >> /etc/fstab
- 删除交换分区
- - swapoff /swapfile
- - rm -rf /swapfile
### 修改mysql配置文件
- vim /et/my.cnf

[client]
port        = 3306

socket      = /tmp/mysql.sock

[mysqld]

port        = 3306

socket      = /tmp/mysql.sock

user = mysql

basedir = /web/server/mysql

datadir = /web/server/mysql/data

pid-file = /web/server/mysql/mysql.pid

log_error = /web/server/mysql/mysql-error.log

slow_query_log = 1

long_query_time = 1

slow_query_log_file = /web/server/mysql/mysql-slow.log

skip-external-locking

key_buffer_size = 32M

max_allowed_packet = 1024M

table_open_cache = 128

sort_buffer_size = 768K

net_buffer_length = 8K

read_buffer_size = 768K

read_rnd_buffer_size = 512K

myisam_sort_buffer_size = 8M

thread_cache_size = 16

query_cache_size = 16M

tmp_table_size = 32M

performance_schema_max_table_instances = 1000

explicit_defaults_for_timestamp = true

\#skip-networking

max_connections = 500

max_connect_errors = 100

open_files_limit = 65535

log_bin=mysql-bin

binlog_format=mixed

server_id   = 232

expire_logs_days = 10

early-plugin-load = ""

default_storage_engine = InnoDB

innodb_file_per_table = 1

innodb_buffer_pool_size = 128M

innodb_log_file_size = 32M

innodb_log_buffer_size = 8M

innodb_flush_log_at_trx_commit = 1

innodb_lock_wait_timeout = 50


[mysqldump]

quick

max_allowed_packet = 16M

[mysql]

no-auto-rehash

[myisamchk]

key_buffer_size = 32M

sort_buffer_size = 768K

read_buffer = 2M

write_buffer = 2M

### 初始化mysql 获取初次密码
- ./mysqld --initialize-insecure --user=mysql --basedir=/web/server/mysql --datadir=/web/server/mysql/data

### 拷贝可执行配置文件
- cp mysql.server /etc/init.d/mysqld

### 启动mysql
- service mysqld start
### 测试连接
- ./mysql -uroot -p

### 修改密码，设置远程登录，创建数据库实例(这里是mysql里语句操作)
 ALTER USER USER() IDENTIFIED BY '123456';
create  user root@'%' identified by '123456';

grant  all privileges on *.* to root@'%';

flush  privileges;

ALTER  USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY '1234567';

flush  privileges;

### 添加环境变量
- vim /etc/profile
- - PATH=/usr/local/mysql/bin:$PATH
- source /etc/profile
### 设置开机启动
- systemctl enable mysqld
