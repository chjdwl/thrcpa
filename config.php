<?php

return [
    'from_type'=>[
        0=>'自营',
        1=>'淘宝客',
        2=>'京东',
    ],
    //淘宝客
    'tbk'=>[
        'android'             => [
            'appkey'    => '',
            'appsecret' => '',
            'appid'     => '',
            'pid'       => [''],    //推广位id
            'min_version' => 225,//佣金显示最低版本
        ],
        'ios'                 => [
            'appkey'    => '',
            'appsecret' => '',
            'appid'     => '',
            'pid'       => [''],    //推广位id
            'min_version' => 180,//佣金显示最低版本
        ],
        'activity'  => [    //官方活动推广位:  zone_id => type
            '109639650284' => 1,
        ],
        /*    'pc'                  => [
                'appkey'    => '27707061',
                'appsecret' => 'b377f44b6046aec0cba001732fae25c2',
                'appid'     => '649350436',
                'pid'       => ['mm_479030055_649350436_109253750216'],    //推广位id
            ],*/
        'member_id'           => '',
        'pull_order_interval' => 180, //同步订单时间间隔 ：单位 （分钟）
        'pull_all_order_time' => '23:30', //每天订单全量同步时间点，修改的话要以30分钟为单位
        'tbk_start_time'      => '2019-08-20 00:00:00', //初始时间
        'channel_invite_code' => 'T7VWGP',
        'user_invite_code'    => 'WJQ66V',

        //淘宝推广分类同步时间配置： 类型名称=>24小时内，隔X小时更新一次（不配置默认晚上0点）
        'remote_type_sync' => [
            '大额券' => 1,
        ],
    ],
    'games'=>[
        'ninetyonetaojin'=>[ //91平台
            'ios'=>[
                'open' => false, //开关 true开启、false关闭
                'title'=>'海量游戏轻松玩
边玩边赚嗨翻天',
                'MtId'=>'', //平台id
                'AppKey'=>'', //平台appkey
                'banner'=>'', //背景图
                'mt_rate' => 0.8,//媒体分成比例
                'request_type' => 'sdk', //跳转方式
                'mini_version'=>0,//最低支持版本
            ],
            'android'=>[
                'open' => false, //开关 true开启、false关闭
                'title'=>'海量游戏轻松玩
边玩边赚嗨翻天',
                'MtId'=>'',
                'AppKey'=>'',
                'banner'=>'',
                'mt_rate' => 0.8,//媒体分成比例
                'request_type' => 'sdk',
                'mini_version'=>0,//最低支持版本
            ],
        ],
        'fengling'=>[ //风铃平台
            'ios'=>[
                'open' => false, //开关 true开启、false关闭
                'title'=>'超有趣的游戏重磅上线
指尖一点 轻松玩赚',
                'partnerId'=>'', //平台id
                'key'=>'', //平台appkey
                'banner'=>'', //背景图
                'mt_rate' => 1,//媒体分成比例
                'request_type' => 'sdk', //跳转方式
                'mini_version'=>0,//最低支持版本
            ],
            'android'=>[
                'open' => false, //开关 true开启、false关闭
                'title'=>'超有趣的游戏重磅上线
指尖一点 轻松玩赚',
                'partnerId'=>'', //平台id
                'key'=>'', //平台appkey
                'banner'=>'',
                'mt_rate' => 1,//媒体分成比例
                'request_type' => 'sdk',
                'mini_version'=>167,//最低支持版本
            ],
        ],
        'pceggs'=>[ //PC蛋蛋
            'ios'=>[
                'open' => false, //开关 true开启、false关闭
                'title'=>'全新游戏火爆来袭
限量体验先到先得！',
                'ptype'=>'1',
                'pid'=>'', //平台id
                'appkey'=>'', //平台appkey
                'newversion'=>'1',
                'banner'=>'', //背景图
                'request_url'=>'http://ifsapp.pceggs.com/Pages/IntegralWall/IW_Awall_adList.aspx',
                'request_type' => 'h5',
                'request_method' => 'get',
                'mt_rate' => 1,//分成比例
                'mini_version'=>0,//最低支持版本
            ],
            'android'=>[
                'open' => false, //开关 true开启、false关闭
                'title'=>'全新游戏火爆来袭
限量体验先到先得！',
                'ptype'=>'2',
                'pid'=>'', //平台id
                'appkey'=>'', //平台appkey
                'newversion'=>'1',
                'banner'=>'', //背景图
                'request_url'=>'http://ifsapp.pceggs.com/Pages/IntegralWall/IW_Awall_adList.aspx',
                'request_type' => 'h5',
                'request_method' => 'get',
                'mt_rate' => 1,//分成比例
                'mini_version'=>0,//最低支持版本
            ],
        ],
    ]
];