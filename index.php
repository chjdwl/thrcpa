<?php
namespace chj\ThrCpa;
function autoloadAdjust(){
    //取消原有加载方法
    $oldAutoloads = spl_autoload_functions();
    if ($oldAutoloads){
        foreach ($oldAutoloads as $autoload){
            spl_autoload_unregister($autoload);
        }
    }
    //加载现在的
    spl_autoload_register(function ($class_name) {
        if (strpos($class_name,'chj\\ThrCpa') !== false ){
            $class_name = str_replace('chj\\ThrCpa','src\\',$class_name);
        }
        $filePath = $class_name . '.php';
        echo $class_name.PHP_EOL;
        if (file_exists($filePath)){
            require_once $filePath;
        }
    });
    //注册框架的
    if (function_exists('__autoload')){
        spl_autoload_register('__autoload');
    }
    //注册原有加载方法
    if ($oldAutoloads){
        foreach ($oldAutoloads as $autoload){
            spl_autoload_register($autoload);
        }
    }
}
autoloadAdjust();
use chj\ThrCpa\Services\TbkGoodsService;
//include 'ThrGoodsFactory.php';
$service = new TbkGoodsService('tbk','ios');
var_dump($service->getGoods());
/*$a = include_once './src/Services/config.php';
var_export($a);*/



