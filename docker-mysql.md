# docker 安装mysql
## 查找镜像：
- docker search mysql
## 下载镜像
- docker pull mysql[--verison]
## 运行镜像
- docker run -p 3307:3306 --name mymysql -v $PWD/conf:/etc/mysql/conf.d -v $PWD/logs:/logs -v $PWD/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 -d mysql
- - -p 3306:3306：将容器的 3306 端口映射到主机的 3306 端口。
   
- - -v $PWD/conf:/etc/mysql/conf.d：将主机当前目录下的 conf/my.cnf 挂载到容器的 /etc/mysql/my.cnf。
   
- - -v $PWD/logs:/logs：将主机当前目录下的 logs 目录挂载到容器的 /logs。
   
- - -v $PWD/data:/var/lib/mysql ：将主机当前目录下的data目录挂载到容器的 /var/lib/mysql 。
   
- - -e MYSQL_ROOT_PASSWORD=123456：初始化 root 用户的密码。
## 查看运行镜像ID
- docker ps 
## 进入镜像
- docker exec -it 62349aa31687 /bin/bash
## 进入mysql更改权限或新增远程连接用户
- 授权 
- - GRANT ALL ON *.* TO 'root'@'%';
- 刷新权限 
- - flush privileges;
- 新增用户 
- - ALTER USER 'root'@'localhost' IDENTIFIED BY 'password' PASSWORD EXPIRE NEVER;
- 更新用户 
- - ALTER USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY '123456';
- 刷新权限 
- - flush privileges;
